#!/bin/bash

# Ensure the script exits if any command fails
set -e

# Check if the correct number of arguments are provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <output_directory>"
    exit 1
fi

# Assign arguments to variables
OUTPUT_DIRECTORY=$1

# Loop through hours 0 to 11
for HOUR in {0..11}; do
    MIDI_OUTPUT_FILE="${OUTPUT_DIRECTORY}/clock-hour-${HOUR}.mid"
    ./render_1h.sh "$HOUR" "$MIDI_OUTPUT_FILE"
done

echo "Generated MIDI, WAV, and MP3 files for all hours from 0 to 11."
