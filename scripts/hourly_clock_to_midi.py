import argparse
import mido
import clock


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate and play a MIDI file.")
    parser.add_argument('--hour', type=int, required=True, help='The current hour')
    parser.add_argument('--outfile', type=str, help='Output file name to save the MIDI')

    args = parser.parse_args()

    clock_builder = clock.RhythmicClockBuilder(
        bpm=120,
        kick_drum=clock.drums['kick_drums']['acoustic_bass_drum'],
        hour_drums=[clock.drums['hi_hats']['open_hi_hat'],
                    clock.drums["cymbals"]["splash_cymbal"]],
        minute_drums=clock.drums['clicks']['metronome_bell']
    )

    rhythmic_clock = clock_builder.build_partial_clock(
            [args.hour],
            range(60),
            range(15)
            )

    # clock.play_midi_file(rhythmic_clock, mido.get_output_names()[1], 2)

    if args.outfile:
        rhythmic_clock.save(args.outfile)
