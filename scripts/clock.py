
from typing import List, Union

import mido

drums = {
    'kick_drums': {
        'acoustic_bass_drum': 35,
        'bass_drum_1': 36
    },
    'cymbals': {
        'crash_cymbal_1': 49,
        'crash_cymbal_2': 57,
        'splash_cymbal': 55,
        'ride_cymbal_1': 51,
        'ride_cymbal_2': 59,
        'chinese_cymbal': 52,
        'ride_bell': 53
    },
    'hi_hats': {
        'closed_hi_hat': 42,
        'open_hi_hat': 46,
        'pedal_hi_hat': 44
    },
    'toms': {
        'low_floor_tom': 41,
        'high_floor_tom': 43,
        'low_tom': 45,
        'low_mid_tom': 47,
        'high_mid_tom': 48,
        'high_tom': 50
    },
    'clicks': {
        'metronome_click': 33,
        'metronome_bell': 34
    }
}

note_to_midi = {
     'C': 0,
     'C#': 1,
     'D': 2,
     'D#': 3,
     'E': 4,
     'F': 5,
     'F#': 6,
     'G': 7,
     'G#': 8,
     'A': 9,
     'A#': 10,
     'B': 11
}


def note_name_to_midi_number(note: str) -> int:
    name = note[:-1]  # Note name without the octave
    octave = int(note[-1])  # Octave number
    return note_to_midi[name] + (octave + 1) * 12


def set_tempo(mid: mido.MidiFile, tempo_bpm: int) -> None:
    """ Sets the tempo of a midi track. """
    microseconds_per_beat = mido.bpm2tempo(tempo_bpm)
    tempo_meta_msg = mido.MetaMessage('set_tempo', tempo=microseconds_per_beat)
    # Insert tempo meta message at the beginning of the track
    for track in mid.tracks:
        track.insert(0, tempo_meta_msg)


def change_instrument(track: mido.MidiTrack, instrument: int, channel: int = 0) -> None:
    # Insert program change message at the beginning of the track
    track.append(mido.Message(
        'program_change',
        program=instrument,
        time=0,
        channel=channel)
        )


def set_volume(track: int, volume: int, channel: int = 0) -> None:
    # Volume range is from 0 to 127
    volume = max(0, min(127, volume))
    # Insert control change message for volume at the beginning of the track
    track.insert(0, mido.Message('control_change',
                                 control=7,
                                 value=volume,
                                 channel=channel,
                                 time=0)
                 )


def calculate_midi_duration_in_ticks(mid: mido.MidiFile) -> int:
    total_ticks = 0

    # Iterate through all tracks
    for track in mid.tracks:
        track_ticks = sum(msg.time for msg in track)
        total_ticks = max(total_ticks, track_ticks)

    return total_ticks


def play_midi_file(mid: mido.MidiFile, outport: str, n_repeats: int = 1) -> None:
    with mido.open_output(outport) as outport:
        for i in range(n_repeats):
            for msg in mid.play():
                outport.send(msg)


minute_patterns = [
    [1, 1, 1, 1],
    [1, 1, 1, 0],
    [1, 1, 0, 0],
    [1, 0, 0, 0],
    [0, 0, 0, 1],
    [0, 0, 1, 0],
    [0, 1, 0, 0],
    [1, 0, 0, 1],
    [0, 0, 1, 1],
    [0, 1, 1, 0],
    [1, 1, 0, 1],
    [1, 0, 1, 0],
    [0, 1, 0, 1],
    [1, 0, 1, 1],
    [0, 1, 1, 1],
]


class RhythmicClockBuilder:

    def __init__(
            self,
            bpm: int,
            kick_drum: int,
            hour_drums: Union[int, List[int]],
            minute_drums: Union[int, List[int]],
            one_beat_in_ticks: int = 480
            ):
        """
        Params:
            kick_drum: MIDI pitch number for the kick drum sound to use.
            hour_drums: MIDI pitch number for the kick drum sound to use.
        """
        self.bpm = bpm
        self.kick_drum = kick_drum
        self.ONE_BEAT = one_beat_in_ticks
        self.HALF_BEAT = one_beat_in_ticks // 2
        self.QUARTER_BEAT = one_beat_in_ticks // 4

        if isinstance(hour_drums, int):
            hour_drums = [hour_drums]
        self.hour_drums = hour_drums

        if isinstance(minute_drums, int):
            minute_drums = [minute_drums]
        self.minute_drums = minute_drums

    def create_melody(self, track: mido.MidiTrack):
        f3h = note_name_to_midi_number("F#3")
        d4 = note_name_to_midi_number("D4")
        b3 = note_name_to_midi_number("B3")
        c3h = note_name_to_midi_number("C#4")

        track.append(mido.Message('note_on', note=f3h, velocity=64, time=0))
        track.append(
                mido.Message('note_off', note=f3h, velocity=64, time=self.QUARTER_BEAT)
                )
        for i in range(3):
            track.append(mido.Message(
                'note_on',
                note=f3h,
                velocity=64,
                time=self.QUARTER_BEAT)
                )
            track.append(mido.Message(
                'note_off',
                note=f3h,
                velocity=64,
                time=self.QUARTER_BEAT)
                )

        track.append(mido.Message(
            'note_on',
            note=d4,
            velocity=64,
            time=2*self.ONE_BEAT+self.QUARTER_BEAT)
            )
        track.append(
            mido.Message('note_off', note=d4, velocity=64, time=self.HALF_BEAT))

        track.append(
            mido.Message('note_on', note=d4, velocity=64, time=self.ONE_BEAT)
            )
        track.append(
            mido.Message('note_off', note=d4, velocity=64, time=self.HALF_BEAT)
            )

        track.append(mido.Message('note_on', note=b3, velocity=64, time=0))
        track.append(
            mido.Message('note_off', note=b3, velocity=64, time=self.HALF_BEAT)
            )

        track.append(
            mido.Message('note_on', note=c3h, velocity=64, time=self.HALF_BEAT)
            )
        track.append(
            mido.Message('note_off', note=c3h, velocity=64, time=self.HALF_BEAT)
            )

        # add a silence to finish the phrase
        track.append(
            mido.Message('note_off', note=0, velocity=0, time=self.HALF_BEAT)
            )

    def create_kick_track(self, track: mido.MidiTrack):
        """ Create the kick drum bass part of the clock.

        """

        ONE_BEAT, QUARTER_BEAT = self.ONE_BEAT, self.QUARTER_BEAT
        sound = self.kick_drum

        track.append(
            mido.Message('note_on', note=sound, velocity=70, time=0, channel=9)
            )
        track.append(mido.Message(
            'note_off',
            note=sound,
            velocity=64,
            time=QUARTER_BEAT,
            channel=9)
            )

        for i in range(3):
            track.append(mido.Message(
                'note_on',
                note=sound,
                velocity=70,
                time=2*ONE_BEAT-QUARTER_BEAT,
                channel=9)
                )
            track.append(mido.Message(
                'note_off',
                note=sound,
                velocity=64,
                time=QUARTER_BEAT,
                channel=9)
                )

        # add a silence to finish the phrase
        track.append(mido.Message(
            'note_off',
            note=0,
            velocity=0,
            time=2*ONE_BEAT-QUARTER_BEAT)
            )

    def create_hourly_track(self, track: mido.MidiTrack, hour: int):
        """ Creates the track with the hour gong
        Params:
            sound2: If set to a positive value, will be layered with the first sound
        """

        ONE_BEAT = self.ONE_BEAT
        HALF_BEAT = self.HALF_BEAT
        QUARTER_BEAT = self.QUARTER_BEAT

        sound = self.hour_drums[0]
        sound2 = self.hour_drums[1] if len(self.hour_drums) > 1 else 0

        hour = hour % 12
        current_time = 2*ONE_BEAT + hour*HALF_BEAT
        track.append(mido.Message(
            'note_on',
            note=sound,
            velocity=90,
            time=2*ONE_BEAT + hour*HALF_BEAT,
            channel=9)
            )
        if sound2 > 0:
            track.append(mido.Message(
                'note_on',
                note=sound2,
                velocity=90,
                time=0,
                channel=9)
                )
        track.append(mido.Message(
            'note_off',
            note=sound,
            velocity=64,
            time=QUARTER_BEAT,
            channel=9)
            )
        current_time += QUARTER_BEAT
        if sound2 > 0:
            track.append(mido.Message(
                'note_off',
                note=sound2,
                velocity=64,
                time=0,
                channel=9)
                )
        # add a silence to finish the phrase
        track.append(mido.Message(
                'note_off',
                note=sound,
                velocity=64,
                time=8*ONE_BEAT - current_time,
                channel=9)
            )

    def create_minute_track(self, track: mido.MidiTrack, minute: int):
        """ Creates the track with the hour gong """

        ONE_BEAT = self.ONE_BEAT
        HALF_BEAT = self.HALF_BEAT

        sound = self.minute_drums[0]
        minute = minute % 60
        quarter = minute // 15
        minute_offset = minute % 15
        pattern = minute_patterns[minute_offset]
        # start at the measure that corresponds to the minute quarter
        # 0-14 -> first measure
        # 15-29 -> second measure
        # ...
        tick_on = mido.Message(
            'note_on',
            note=sound,
            velocity=80,
            time=0,
            channel=9)
        tick_off = mido.Message(
            'note_off',
            note=sound,
            velocity=80,
            time=0,
            channel=9)

        track.append(mido.Message(
            'note_off',
            note=sound,
            velocity=80,
            time=quarter*2*ONE_BEAT,
            channel=9)
            )
        current_time = quarter*2*ONE_BEAT
        for this_half_note in pattern:
            # if there is no note we use the note off msg as a silence
            if this_half_note:
                track.append(mido.Message(
                    'note_on',
                    note=sound,
                    velocity=80,
                    time=0,
                    channel=9)
                    )
                # track.append(mido.Message('note_on', note=drum_sounds['toms']['low_floor_tom'], velocity=55, time=0, channel=9))
                track.append(mido.Message(
                    'note_on',
                    note=77,
                    velocity=85,
                    time=0,
                    channel=9)
                    )
                # track.append
            track.append(mido.Message(
                'note_off',
                note=sound,
                velocity=64,
                time=HALF_BEAT,
                channel=9)
                )
            current_time += HALF_BEAT

        # add a silence until end of phrase
        track.append(mido.Message(
            'note_off',
            note=sound,
            velocity=64,
            time=8*ONE_BEAT - current_time,
            channel=9))

    def build_full_clock(self) -> mido.MidiFile:
        return self.build_partial_clock()

    def build_partial_clock(
            self,
            hour_range: List[int] = list(range(12)),
            minute_range: List[int] = list(range(60)),
            per_minute_repeats: List[int] = list(range(15))
            ) -> mido.MidiFile:
        # Compiling the clock
        mid = mido.MidiFile()
        melody = mido.MidiTrack()
        kick = mido.MidiTrack()
        hour_gong = mido.MidiTrack()
        minute_hand = mido.MidiTrack()
        mid.tracks.append(melody)
        mid.tracks.append(kick)
        mid.tracks.append(hour_gong)
        mid.tracks.append(minute_hand)

        mid.ticks_per_beat = self.ONE_BEAT
        assert self.bpm == 120, "Only 120bmp supported for now"
        set_tempo(mid, self.bpm)
        SYNTH_CHARANG = 85
        change_instrument(melody, SYNTH_CHARANG)
        set_volume(hour_gong, 127, 9)

        for hour in hour_range:
            for minute in minute_range:
                for repeat in per_minute_repeats:
                    self.create_melody(melody)
                    self.create_kick_track(kick)
                    self.create_hourly_track(hour_gong, hour)
                    self.create_minute_track(minute_hand, minute)

        return mid
