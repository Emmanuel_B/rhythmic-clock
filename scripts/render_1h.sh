#!/bin/bash

# Ensure the script exits if any command fails
set -e

# Check if the correct number of arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <hour> <midi_output_file>"
    exit 1
fi

HOUR=$1
MIDI_OUTPUT_FILE=$2
SOUNDFONT="/usr/share/sounds/sf2/FluidR3_GM.sf2"


# Define the names for the WAV and MP3 files
WAV_OUTPUT_FILE="${MIDI_OUTPUT_FILE%.mid}.wav"
MP3_OUTPUT_FILE="${MIDI_OUTPUT_FILE%.mid}.mp3"
MONO_WAV_OUTPUT_FILE="${MIDI_OUTPUT_FILE%.mid}_mono.wav"


# Run the Python script to generate the MIDI file
python hourly_clock_to_midi.py --hour "$HOUR" --outfile "$MIDI_OUTPUT_FILE"

# Convert the MIDI file to WAV using FluidSynth
fluidsynth -ni "/usr/share/sounds/sf2/FluidR3_GM.sf2" "$MIDI_OUTPUT_FILE" -F "$WAV_OUTPUT_FILE" -r 22050

# Convert the stereo WAV file to mono using ffmpeg
ffmpeg -i "$WAV_OUTPUT_FILE" -ac 1 "$MONO_WAV_OUTPUT_FILE"

# Convert the WAV file to MP3 using ffmpeg
ffmpeg -i "$MONO_WAV_OUTPUT_FILE" -codec:a libmp3lame -b:a 64k "$MP3_OUTPUT_FILE"

echo "Generated MP3 file: $MP3_OUTPUT_FILE"
